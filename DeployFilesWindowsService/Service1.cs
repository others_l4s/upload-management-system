﻿using CommonService;
using Microsoft.Web.Administration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.ServiceProcess;
using CommonService.Extensions;

namespace DeployFilesWindowsService
{
	public partial class Service1 : ServiceBase
	{
		public Service1()
		{
			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			FileSystemWatcher watcher = new FileSystemWatcher();
			watcher.Path = ConfigurationManager.AppSettings["DeployFileCommandWatcherFolderPath"];
			watcher.NotifyFilter = NotifyFilters.FileName;
			watcher.Filter = "*.txt";

			// Add event handlers.
			watcher.Created += new FileSystemEventHandler(OnChanged);
			// Begin watching.
			watcher.EnableRaisingEvents = true;

			GetAllSiteNames();
		}

		protected override void OnStop()
		{
		}

		private void OnChanged(object sender, FileSystemEventArgs e)
		{
			ReadFileAndProcess(e.FullPath);
		}

		#region ' Private Methods'

		//Read command file and execute as per command 
		private void ReadFileAndProcess(string filePath)
		{
			try
			{
				System.Threading.Thread.Sleep(1000);
				string[] commandTextValues = File.ReadAllLines(filePath);
				if (commandTextValues.Length < 1)
				{
					return;
				}
				var command = Convert.ToInt32(commandTextValues[0]);
				switch (command)
				{
					case (int)Commands.UploadFiles:
						var siteName = commandTextValues[1];
						var zipPath = commandTextValues[2];
						var isReplace = commandTextValues[3] == "1";
						var isIgnoreRootFolder = commandTextValues[4] == "1";
						var isReplaceIgnoreFiles = commandTextValues[5] == "1";
						UploadFolderToSite(siteName, zipPath, isReplace, isIgnoreRootFolder, isReplaceIgnoreFiles);
						break;
					case (int)Commands.UpdateSiteList:
						GetAllSiteNames();
						break;
				}
				File.Delete(filePath);
			}
			catch (Exception ex)
			{
				ErrorLog(ex.Message.ToString());
			}
		}

		//Upload Files to folder
		private void UploadFolderToSite(string siteNames, string zipName, bool isReplace, bool isIgnoreRootFolder, bool isReplaceIgnoreFiles)
		{
			try
			{
				var siteNameList = siteNames.Split(',');
				string zipPath = Path.Combine(ConfigurationManager.AppSettings["ZipFolderPath"], zipName);
				foreach (var siteName in siteNameList)
				{
					using (ServerManager iisManager = new ServerManager())
					{
						string siteNameListString = string.Empty;
						Site site = iisManager.Sites[siteName];
						if (site != null)
						{
							try
							{
								site.Stop();
								string uploadPath = site.Applications["/"].VirtualDirectories["/"].PhysicalPath;
								//Take backup of Hosted folder
								var currentDateTime = DateTime.Now;
								string backupZipName = currentDateTime.Year + "_" + currentDateTime.Month + "_" + currentDateTime.Day + " " + currentDateTime.Hour + "_" + currentDateTime.Minute + "_" + currentDateTime.Second + ".zip";
								string backpZipPath = Path.Combine(ConfigurationManager.AppSettings["SiteBackupFolderPath"], siteName);
								if (!Directory.Exists(backpZipPath))
								{
									Directory.CreateDirectory(backpZipPath);
								}
								backpZipPath = Path.Combine(backpZipPath, backupZipName);
								ZipFile.CreateFromDirectory(uploadPath, backpZipPath);

								//Upload new files
								var ignoreFileList = GetIgnoreFileNames();
								if (isReplace)
								{
									DeleteDirectory(uploadPath, true, isReplaceIgnoreFiles, ignoreFileList);
								}
								using (var archive = ZipFile.Open(zipPath, ZipArchiveMode.Read))
								{
									archive.ExtractToDirectory(uploadPath, true, ignoreFileList, isIgnoreRootFolder);
								}
								Log($"{siteName} latest uploaded successfully");
							}
							catch (Exception ex)
							{
								ErrorLog(ex.Message.ToString());
							}
							finally
							{
								site.Start();
							}
						}
					}
				}
				File.Delete(zipPath);
			}
			catch (Exception ex)
			{
				ErrorLog(ex.Message.ToString());
			}
		}

		//Get All Site name
		private void GetAllSiteNames()
		{
			try
			{
				using (ServerManager iisManager = new ServerManager())
				{
					var siteNameListString = string.Empty;
					foreach (var site in iisManager.Sites)
					{
						siteNameListString += $"{site.Name + Environment.NewLine}";
					}
					UpdateSiteNameList(siteNameListString);
				}
			}
			catch (Exception ex)
			{
				ErrorLog(ex.Message.ToString());
			}
		}

		//Save execution log
		private void Log(string text)
		{
			var logTextFilePath = Path.Combine(ConfigurationManager.AppSettings["LogFolderFilePath"], "log.txt");
			File.AppendAllText(logTextFilePath, $"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} - {text + Environment.NewLine}");
		}

		//Save error log
		private void ErrorLog(string text)
		{
			var logTextFilePath = Path.Combine(ConfigurationManager.AppSettings["LogFolderFilePath"], "errorlog.txt");
			File.AppendAllText(logTextFilePath, $"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} - {text + Environment.NewLine}");
		}

		//Save Site name list to text file
		private void UpdateSiteNameList(string text)
		{
			var sitenameListFilePath = Path.Combine(ConfigurationManager.AppSettings["LogFolderFilePath"], "sitenamelist.txt");
			if (File.Exists(sitenameListFilePath))
			{
				File.Delete(sitenameListFilePath);
			}
			File.AppendAllText(sitenameListFilePath, text);
		}

		//Get Ignore File name

		private List<string> GetIgnoreFileNames()
		{
			List<string> list = new List<string>();
			try
			{
				list = File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + @"\IgnoreFiles.txt").Select(x => x.Trim()).ToList();
			}
			catch (Exception ex)
			{
				ErrorLog(ex.Message.ToString());
			}
			return list;
		}


		private void DeleteDirectory(string target_dir, bool isParent, bool isReplaceIgnoreFiles, List<string> ignoreFiles)
		{
			string[] files = Directory.GetFiles(target_dir);
			string[] dirs = Directory.GetDirectories(target_dir);

			foreach (string file in files)
			{
				var fullName = file.Split('\\').Last();
				if (!isReplaceIgnoreFiles && ignoreFiles.Contains(fullName))
				{
					continue;
				}
				File.SetAttributes(file, FileAttributes.Normal);
				File.Delete(file);
			}

			foreach (string dir in dirs)
			{
				DeleteDirectory(dir, false, isReplaceIgnoreFiles, ignoreFiles);
			}

			if (!isParent)
			{
				if (!Directory.EnumerateFileSystemEntries(target_dir).Any())
				{
					Directory.Delete(target_dir);
				}
			}
		}
		#endregion
	}
}
