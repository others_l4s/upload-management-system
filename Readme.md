# Deploy Files to Server

## Requirements
  - dotnet core hosting package for 3.1

## Configuration Steps

### 1. Install Windows Service
- Create one folder name  `Logs`-
    - Create four folder under that
        - DeployZipLogs
        - DeployZipCommand
        - DeployZipUpload
        - SiteBackupFolder

- Set key value in app.config.
    - Created folder path set in app.config
    - `<add key="LogFolderFilePath" value="E:\DeployZip\DeployZipLogs"/>`
    - `<add key="DeployFileCommandWatcherFolderPath" value="E:\DeployZip\DeployZipCommand"/>`
    - `<add key="ZipFolderPath" value="E:\DeployZip\DeployZipUpload"/>`
    - `<add key="SiteBackupFolderPath" value="E:\DeployZip\SiteBackupFolder"/>` 
- Build Solution `DeployFilesToServer.sln`
- Copy `DepoyFilesToServer\DeployFilesWindowsService\bin\Debug` to server
    e.g `C:\HostedFiles\UploadFilesManagment\WindowsService`
- If you already have build folder then you directly skip above steps and in that folder you can see app.config and change folder path in that config file.
- Open cmd prompt with run as administrator
- Type below command with windows service exe path
- `sc create DeployFilesService binpath= "C:\HostedFiles\UploadFilesManagment\WindowsService\DeployFilesWindowsService.exe"`
- `sc start DeployFilesService`
- Start service from `services.msc`

### 2. Run Website
- Build Solution `DeployFilesToServer.sln`
- Set value in appsettings.json
    - `"DeployFileCommandWatcherFolderPath": "E:\\DeployZip\\DeployZipCommand"`
    - `"LogFolderFilePath": "E:\\DeployZip\\DeployZipLogs"`
    - `"ZipFolderPath": "E:\\DeployZip\\DeployZipUpload"`
    - `"LoginUsername": "admin"` - website credentails
    - `"LoginPassword": "123"`
- Run website or Host website to IIS
- For Host to IIS
    - open command prompt in `DepoyFilesToServer\DeployFileWebApp` folder
    - Run `dotnet publish` command
    - host `bin\Debug\netcoreapp2.2\publish` folder to IIS
    - if you already have publish folder than directly host that folder to site

### 3. Upload Files to Server steps
 - After running website, you can see hoated website list
 - select website from list
 - create zip of your changes files and select in upload zip
 - then click upload
 - After sometimes you will see logs in below Logs listing grid