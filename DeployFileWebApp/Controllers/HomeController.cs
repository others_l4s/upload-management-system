﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DeployFileWebApp.Models;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Http;
using System.IO;
using CommonService;

namespace DeployFileWebApp.Controllers
{
	public class HomeController : Controller
	{
		private readonly AppSettings _appSettings;
		private const string _loginSessionKeyName = "_Username";

		private readonly ILogger<HomeController> _logger;

		public HomeController(ILogger<HomeController> logger, IOptions<AppSettings> options)
		{
			_appSettings = options.Value;
			_logger = logger;
		}

		public IActionResult Index()
		{
			var username = HttpContext.Session.GetString(_loginSessionKeyName);
			if (string.IsNullOrEmpty(username))
			{
				return RedirectToAction("Index", "Login");
			}

			GetSiteNameList();

			ViewData["IsLogin"] = true;

			var lines = GetLogs();
			var model = new DataModel()
			{
				Logs = lines,
				ErrorLogs = GetErrorLogsData(),
				IsIgnoreRootFolder = true
			};
			return View(model);
		}

		public IActionResult Privacy()
		{
			return View();
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}

		public ActionResult Logout()
		{
			HttpContext.Session.Remove(_loginSessionKeyName);
			return RedirectToAction("Index", "Login");
		}

		[HttpPost]
		[RequestFormLimits(MultipartBodyLengthLimit = 209715200)]
		[RequestSizeLimit(209715200)]
		public ActionResult UploadZip(DataModel dataModel)
		{
			if (ModelState.IsValid)
			{
				if (dataModel.File != null)
				{
					var file = dataModel.File;
					var extension = file.FileName.Split(".").Last().ToLower();
					var fileName = DateTime.Now.ToFileTime() + "." + extension;
					string filePath = _appSettings.ZipFolderPath + @"\" + fileName;
					using (FileStream fs = System.IO.File.Create(filePath))
					{
						file.CopyTo(fs);
						fs.Flush();
					}
					var arguments = fileName + Environment.NewLine + (dataModel.IsReplace ? "1" : "0") + Environment.NewLine + (dataModel.IsIgnoreRootFolder ? "1" : "0") + Environment.NewLine + (dataModel.IsReplaceIgnoreFiles ? "1" : "0");
					var sitenames = string.Join(",", dataModel.SiteName);
					PutCommand((int)Commands.UploadFiles, sitenames, arguments);
					return this.GetJsonResult("File upload command sent successfully. Please wait for few minutes.");
				}
				return this.GetErrorJsonResult("Please select zip file");
			}
			else
			{
				return this.GetJsonResult();
			}
		}

		[HttpPost]
		public ActionResult UpdateSiteList()
		{
			PutCommand((int)Commands.UpdateSiteList);
			return RedirectToAction("Index", "Home");
		}


		[HttpGet]
		public ActionResult GetLogList()
		{
			return StatusCode(200, GetLogs());
		}

		[HttpGet]
		public ActionResult GetErrorLogs()
		{
			return StatusCode(200, GetErrorLogsData());
		}

		#region ' Private Methods'

		private string[] GetLogs()
		{
			string[] lines = new string[100];
			try
			{
				lines = System.IO.File.ReadAllLines(_appSettings.LogFolderFilePath + @"\log.txt");
				Array.Reverse(lines);
				lines = lines.Take(10).ToArray();
			}
			catch
			{

			}
			return lines;
		}

		private string[] GetErrorLogsData()
		{
			string[] lines = new string[100];
			try
			{
				lines = System.IO.File.ReadAllLines(_appSettings.LogFolderFilePath + @"\errorlog.txt");
				Array.Reverse(lines);
				lines = lines.Take(10).ToArray();
			}
			catch
			{

			}
			return lines;
		}
		private void PutCommand(int command, string extraArgument = "", string extraArgument1 = "")
		{
			Guid guid = Guid.NewGuid();
			var path = _appSettings.DeployFileCommandWatcherFolderPath + @"\" + guid.ToString() + ".txt";
			System.IO.File.AppendAllText(path, command.ToString() + Environment.NewLine + extraArgument + Environment.NewLine + extraArgument1);
		}

		private void GetSiteNameList()
		{
			List<string> list = new List<string>();
			try
			{
				list = System.IO.File.ReadAllLines(_appSettings.LogFolderFilePath + @"\sitenamelist.txt").OrderBy(x => x).ToList();
			}
			catch
			{

			}
			ViewBag.ListOfSites = list;
		}

		#endregion
	}
}
