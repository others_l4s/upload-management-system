﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DeployFileWebApp.Models
{
	public class DataModel
	{
		[Required(ErrorMessage = "SiteName is required")]
		public List<string> SiteName { get; set; }

		[Required(ErrorMessage = "File is required")]
		//[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.zip)$", ErrorMessage = "Only .zip extension files are allowed.")]
		public IFormFile File { get; set; }
		public string[] Logs { get; set; }
		public string[] ErrorLogs { get; set; }

		public bool IsReplace { get; set; }
		public bool IsIgnoreRootFolder { get; set; }

		public bool IsReplaceIgnoreFiles { get; set; }
	}
}
