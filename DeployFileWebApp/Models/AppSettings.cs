﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeployFileWebApp.Models
{
	public class AppSettings
	{
		public string DeployFileCommandWatcherFolderPath { get; set; }
		public string LogFolderFilePath { get; set; }
		public string LoginUsername { get; set; }
		public string LoginPassword { get; set; }
		public string ZipFolderPath { get; set; }
	}
}
