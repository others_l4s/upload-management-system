﻿using Microsoft.AspNetCore.Mvc;

namespace DeployFileWebApp.Models
{
	public static class Common
	{
		public static JsonResult GetJsonResult(this Controller controller, string message)
		{
			var jsonResult = controller.Json(new ApiErrorModel()
			{
				Success = true,
				ReturnMsg = message
			});
			jsonResult.StatusCode = 200;
			return jsonResult;
		}

		public static JsonResult GetErrorJsonResult(this Controller controller, string errorMessage)
		{
			var message = new ApiErrorModel()
			{
				Success = false,
				ReturnMsg = errorMessage
			};
			message.Errors.Add(errorMessage);
			var jsonResult = controller.Json(message);
			jsonResult.StatusCode = 422;
			return jsonResult;
		}
		public static JsonResult GetJsonResult(this Controller controller)
		{
			var apiError = new ApiErrorModel(controller.ModelState);
			var jsonResult = controller.Json(apiError);
			jsonResult.StatusCode = 422;
			return jsonResult;
		}
	}
}
